# RedACTO Base theme

This is a bootstrap starterkit skeleton theme.

Use this theme as a template to create new child theme based on bs_bootstrap parent theme.

### Install

Copy to /themes/custom or other folder as desired and rename all occurrences of starterkit_bootstrap with your theme name.

If you are following standard Drupal positions of contrib and custom themes then default parentTheme paths in gulp-options.yml will work for you, if not then then adjust accordingly.

## Documentation

See bs_base/README.md
See bs_bootstrap/README.md
