/**
 * @file
 * Provides Gulp configurations and tasks for compiling bs_bootstrap
 * CSS files from SASS files.
 *
 * We are mostly reusing configurations from bs_base module, creating new
 * tasks for child theme and making sure we create nice gulp task groups so
 * developer UX is nice.
 */

'use strict';

// Load gulp and needed lower level libs.
var gulp = require('gulp');
var yaml = require('js-yaml');
var fs = require('fs');
var merge = require('deepmerge');

// Load gulp options first from this theme.
// @note - Be sure to define proper paragraphsPath relative path first in
// gulp-options.yml. Most of the time default provided path is OK.
var options = yaml.safeLoad(fs.readFileSync('./gulp-options.yml', 'utf8'));

// Deep merge with gulp options from parent themes.
for (var theme of options.parentTheme) {
  var parentThemeOptions = yaml.safeLoad(fs.readFileSync(theme.path + 'gulp-options.yml', 'utf8'));
  options = merge(parentThemeOptions, options);
}

// Add parent path of parent themes so we can do simple
//
//   @import "bs_base/sass/init";
//   @import "bs_bootstrap/sass/init";
//
// in our sass files.
for (var theme of options.parentTheme) {
  options.sass.includePaths.push(theme.path + '../');
}

// Automatic lazy loading of gulp plugins.
var plugins = require('gulp-load-plugins')(options.gulpPlugins);

// Load gulp tasks from parent theme and this theme.
for (var theme of options.parentTheme.reverse()) {
  require(theme.path + 'gulp-tasks.js')(gulp, plugins, options);
}
require('./gulp-tasks.js')(gulp, plugins, options);
