/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 *
 */
(function ($) {
  'use strict';

  // Bootstrap popover behavior
  Drupal.behaviors.popoverBehavior = {
    attach: function (context, settings) {
      $('[data-toggle="popover"]').popover();
    }
  };

  // Bootstrap tooltip behavior
  Drupal.behaviors.tooltipBehavior = {
    attach: function (context, settings) {
      $('[data-toggle="tooltip"]').tooltip();
    }
  };
}(jQuery));
