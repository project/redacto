<?php

use Drupal\DrupalExtension\Context\RawDrupalContext;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Tester\Exception\PendingException;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends RawDrupalContext implements SnippetAcceptingContext {

  /**
   * Initializes context.
   *
   * Every scenario gets its own context instance.
   * You can also pass arbitrary arguments to the
   * context constructor through behat.yml.
   */
  public function __construct() {
  }

  /**
   * @Given /^I toggle dropdown$/
   */
  public function iToggleDropdown() {
    $toggle_button_xpath = '//ul/li[contains(@class, "dropbutton-toggle")]/button';
    $this->getSession()->getPage()->find('xpath', $toggle_button_xpath)->click();
  }

  /**
   * @Given /^I wait "([^"]*)" seconds$/
   */
  public function iWaitSeconds($seconds) {
    sleep($seconds);
  }

  /**
   * @Then /^I should see the raw text "([^"]*)"$/
   */
  public function iShouldSeeTheRawText($text) {
    $text = str_replace("'", '"', $text);
    $text = \Drupal::token()->replace($text);
    $content = $this->getSession()->getDriver()->getContent();
    if (strpos($content, $text) === FALSE) {
      throw new \Exception("Text " . $text . " not found");
    }
  }

}
