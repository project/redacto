@api @javascript
Feature: #1 Create redacto_page with paragraphs
  As an administrator,
  I want to create a redacto_page

  Background:
    Given I am logged in as a user with the "administrator" role

  Scenario: Create redacto_page
    Given I am at "node/add"
    And I follow "Page"
    And I fill in "TestRedACTOParagraph" for "Title"
    And I toggle dropdown
    And I press "Add Button"
    And I fill in "http://example.com" for "URL"
    Then I wait for AJAX to finish
    And I fill in "Bootstrap Button" for "Link text"
    And I click "Behavior"
    And I select "Button" from "Style"
    And I press "Save and publish"
    Then I should see "Page TestRedACTOParagraph has been created."
    And I should see text matching "TestRedACTOParagraph"
