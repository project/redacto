@api @javascript
Feature: #3 Create instagram media
  As an administrator,
  I want to create and delete a instagram media

  Background:
    Given I am logged in as a user with the "administrator" role

  Scenario: Create and delete instagram media
    Given I am at "media/add"
    And I follow "Instagram"
    And I fill in "Drupal Con Instagram" for "Media name"
    And I fill in "https://www.instagram.com/p/owhOe1DguN" for "Instagragram url"
    And I press "Save and publish"
    Then I should see "Instagram Drupal Con Instagram has been created."
    And I should see the raw text "<iframe class='instagram-media instagram-media-rendered'"
