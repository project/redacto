@api @javascript
Feature: #2 Create twitter media
  As an administrator,
  I want to create and delete a twitter media

  Background:
    Given I am logged in as a user with the "administrator" role

  Scenario: Create and delete twitter media
    Given I am at "media/add"
    And I follow "Twitter"
    And I fill in "Vienna Drupal Con" for "Media name"
    And I fill in "https://twitter.com/drupalcon/status/857697209130921984" for "Twitt Url"
    And I press "Save and publish"
    Then I should see "Twitter Vienna Drupal Con has been created."
    And I should see the raw text "twitterwidget"
    And I should see the raw text "class='twitter-tweet twitter-tweet-rendered'"
