@api @javascript
Feature: #4 Create video media
  As an administrator,
  I want to create and delete a video media

  Background:
    Given I am logged in as a user with the "administrator" role

  Scenario: Create and delete video media
    Given I am at "media/add"
    And I follow "Video"
    And I fill in "Baltimore Drupal Con" for "Media name"
    And I fill in "https://www.youtube.com/watch?v=q25eaJHpXFo&t" for "Video URL"
    And I press "Save and publish"
    Then I should see "Video Baltimore Drupal Con has been created."
    And I should see the raw text "class='video-embed-field-responsive-video'"
